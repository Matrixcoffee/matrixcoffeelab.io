import _builder
import _config
import sys

import markdumb

text = """
# Futile

adj. [descriptive of attempts]

1. to change the WORLD by participating in it (at school, at work or at the ballot box)

2. to make a peacful, connected society with violent separate selves

3. to create less centralisation by creating more centralisation

4. to make yourself feel what you are not feeling (with thought or emotion)

5. to use CHARITY to cure poverty

6. to use CAPITALISM to cure global environmental meltdown, catastrophic inequality or BOREDOM

7. to transform an unsatisfactory self through the efforts of the same self (i.e. by splitting yourself into three; _self one_ [the unhappy observing self], declaring that _self two_ [the deficient self that is being observed] will, at some point in the future, turn into _self three_ [the unselfish, honest self that self number one wants to become])

8. to sure the sickness, guilt, anxiety and WANT caused by ADDICTION with more NARCOTICS

9. to cure the sickness, stupidity, crime, boredom and waste created by a media, school, prison, hospital and energy-dependent 'society' with more doctors, POLICE, prisons, schools, technology or energy

10. to refute scientism with just facts or logic

11. to argue with, or have to beat, BELIEF

12. to make REALITY fit a PLAN

13. to try to kill a tree by hacking around in its branches

14. (3 chinese characters)

* leads to maniac enmeshment in SOLID-CONSCIOUS FUTILITY TRAPS

From [The Apocalypedia](https://expressiveegg.org/portfolio/apocalypedia/)

~
""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

def paragraphize(iterable):
	for paragraph in iterable:
		if paragraph.startswith("<"): yield paragraph
		else: yield "<p>" + paragraph + "</p>\n"

if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	config['sections'] = [paragraphs]

	main(config)
