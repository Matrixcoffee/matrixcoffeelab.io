import _config
from _template import tag, apply_template
import itertools
import os
import sys

import markdumb

text = """
# Local Mirror

Here are some items I consider important enough to keep a local copy of (in case the original website disappears).

~
""".strip()


def main(config, sections):
	for line in apply_template(config, sections):
		print(line)


def paragraphize(iterable):
	for paragraph in iterable:
		if isinstance(paragraph, str) and not paragraph.startswith("<"):
			yield tag("p", flow=True).append(paragraph)
		else: yield paragraph
		yield ""


def extract_title_and_summary(filename):
	title = None
	summary = None

	with open(filename, "r") as f:
		for rline in f:
			line = rline.strip()

			if title is None:
				if line.startswith("# "):
					title = line[2:].strip()
			elif line.startswith(("#", "\"")):
				break
			elif line != "":
				summary = line
				break

	return (filename, title, summary)


def joinpath(path, filename):
	if not path: return filename
	else: return os.path.join(path, filename)


def generate_index(dirname=None):
	dir_contents = os.scandir(dirname)

	for thing in dir_contents:
		if thing.name != "index.py" and thing.name.endswith(".py") and thing.is_file():
			yield extract_title_and_summary(joinpath(dirname, thing.name))


def format_title_and_summary(filename, title, summary):
	if not title: title = filename
	filename = markdumb.html_escape(os.path.basename(filename[:-3] + ".html"), quote=True)
	title = markdumb.html_escape(title)
	if summary: summary = markdumb.as_html(summary)

	fmt = """<a href="{0}">{1}</a>"""
	if summary: fmt += ": {2}"

	return fmt.format(filename, title, summary)


def generate_html_index(dirname=None):
	yield "<li>"
	for items in generate_index(dirname):
		yield format_title_and_summary(*items)
	yield "</li>"


if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])
	paragraphs = itertools.chain(paragraphs, generate_html_index("mirror"))

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	main(config, paragraphs)
