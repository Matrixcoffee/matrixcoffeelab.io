import _builder
import _config
import sys

import markdumb

text = """
# Self-Mastery: Empathy Enhancement and Consciousness Softening

All problems are caused by **the self-informed self**, which is to say by the mechanism of the thinking-emoting mechanism I normally call 'I' — the **self** — receiving inspiration, intelligence and instinct not from the source of these things — the _unselfish_ **consciousness** and **context** — but _from_ the self.

When mind tells mind what's right or what to do, when emotion tells emotion what I want or don't want, the result is an endless self-replicating nightmare of addiction, frustration, fear, ignorance, despair and [madness](https://expressiveegg.org/2017/03/07/schizophrenic-psychocrat/).

Originally, and sanely, self was **a tool** — a means to focus and represent experience — but, as the power of the self grew, it [reached a point, like all other tools, where it started to require more energy and attention to maintain than it provided](https://expressiveegg.org/2017/01/01/duck-rabbit-duality-paradox-origins-civilisation/"). It took over my conscious experience and began calling itself 'I'. The word we ordinarily use for this usurping entity is **ego**.

Self-mastery then is the eradication of ego — which is to say of self-*as*-master. This is achieved through techniques of **attention softening** and spontaneous **apt-action**, until self is in its place; back on the tool-shelf where it belongs. This does not mean, of course, that I have no self or no sense of self — no fear, no thought, no desire — but that I experience, or can call on, an encompassing **unself-consciousness** that precedes self; and so can act (or interact) despite 'my' fears, pains, doubts, addictions and intrusive thoughts. There is no other skill as valuable, or as practical, as this. All anxiety, boredom, suffering, shame, doubt, confusion, anger and even clumsiness and bad luck are caused by a thinking-wanting-not-wanting self that won't fade back into the void of the conscious context.

## SELF-HELP, SELF-RESPECT AND SELF-BELIEF ARE SELFISH

Only **unself** can solve the problems of self. Subversion, study, creation, co-operation and so on are, as we shall see, an integral part of overcoming ego, but by themselves they cannot solve our individual or collective ills. Anyone who has ever tried to use the mind to stop worrying, or indulged in their emotions in an attempt to overcome them, or tried to work out how to deal with a broken heart knows that using self to solve the problems of self is the quintessence of [futility](self-mastery-empathy-enhancement-consciousness-softening/futile.html).

![excerpt from The Apocalypedia](73.jpg)

From [The Apocalypedia](https://expressiveegg.org/portfolio/apocalypedia/)

Self will take all the love, all the respect, all the belief you (or your therapist) give it, and will demand more. Self is _never_ satisfied, nothing is _ever_ enough for it. All the prestige, all the power, all the possessions in the world aren't enough for it. Only drugs and satiety can quiet its anxious hunger, its restless discontent — and only then for a pitifully short time.

## SELF MASTERY: I AM NOT QUITE A PIANO

Self — the mechanism which I use to think and want — goes insane when faced with [death](https://expressiveegg.org/2017/01/07/postcard-from-the-void/), silence, innocence, uncertainty, creativity, [unconditional love](https://expressiveegg.org/2016/11/10/unconditional-love/), peace of mind, conscious experience of the reality of the present moment or anything else which is, ultimately, not self. It is in such selfless experience that mastery of the usurping mechanism that calls itself 'me' begins. By experiencing the present moment fully, melting into the dark, living in the wild, facing death, accepting pain (even as you move to rid yourself of it) and placing your attention in full-body sense there is relief from the mental-emotional momentum of the self which cannot feed on such thoughtless fodder. Likewise in 'stepping back' from the world that feeds the self, something other than mind and emotions is allowed to breathe, to operate.

_Self-mastery, then, is not self-control_ (using some thoughts and emotions to control other thoughts and emotions) but practising being that which is deeper, more intelligent or more fundamentally true than the surface thinking-emoting mechanism I normally call 'me'. And as self _is_ a mechanism, mastering it is like mastering any other tool, such as the piano. You can't just _decide_ to be a great pianist — such an approach leads to immediate frustration and the idiotic conclusion _'I'm just no good at it.'_ You need to practice for many years; but with some composure you can learn and have fun surprisingly quickly.

But the self is also, of course, unlike a piano; firstly because it goes everywhere with you, even into dream. It must therefore be mastered everywhere. And secondly, the self is alive. It is clever and intelligently resists being mastered. It will use every trick in the book to subvert your attempts to overcome it.

And the book it uses, is you.

### practice

* Watching the mind. Whenever you find yourself automatically thinking (wanting, worrying), practice watching the thoughts as you would do a cinema screen. Practice being the observer of the thoughts, rather than being lost in them.

* Being the body. _The body, ultimately, is more intelligent than the mind_ ([which is why women, ultimately, are more intelligent than men](https://expressiveegg.org/2017/03/29/the-tao-of-gender/)). Practice breathing. Practice feeling the subtle ('[apperceptive](https://expressiveegg.org/portfolio/apocalypedia/)') sensation of life that is in the whole body — in the thighs, the arms, the belly, the corners of the mouth. Do this not because it is 'spiritual' but because it feels good. There is always peace in the body, always relief, always presence. The mind and the emotions always rush off into the past and future, but the body is always here. Here there is no problem you can't deal with — there, there is no problem you can.

* Walking through the metro enjoying your breathing more than the adverts, walking through the city experiencing your body rather than the shop fronts. Master the urge to look at flashing lights and fluttering flags by experiencing the simplest sensations within the body system.

* Refusing to participate in gossip, looking out for that little clench of excitement when gossipy news is dangled before you. Let go of the continual _'like / dislike / mmm really like that / ooh that's no good / would love to see that show / oh dear what a fool…'_ reportage that the mind bounces back from typical city drift. See how pornographic news-violence has power over your attention — and in seeing this, take the power back. Allow the urge to rubber-neck disaster slip through your system. Return again to the body.

* Likewise turn your fucking phone off. The smart-phone (and computer) is an extension of the thinking mind.

* Eating when you eat. Walking when you walk, showering when you shower, kissing when you kiss… and so on. Fully experience what you are doing and watch the urge to leave what is, the urge to focus on something, think about it, think about that, and about that… and lost again. Fidgeting, picking, giving in to addictive urges, responding mechanically, biting your tongue or lip while eating, dropping things, losing things and realising that you can't remember anything that happened during your journey are all signs of inner death.

* Letting go of moodies and refusing to blame others for them. Not easy. Have you ever tried to say, right in the middle of a full mood-on, _'yes, I am being pointlessly irritable right now.'_? It takes more presence and courage than kendo.

* Likewise, when someone crosses you, when someone accuses you of something you can't possibly be guilty of, when someone attacks you for no reason — are you man enough or woman enough to let it pass through you? To come back at them with cheer? To meet hatred with friendliness? Such nobility wilts Little Me, which must always be right, always justified, always victorious.

* Gratitude. Self is never grateful — all it understands is debt and guilt. But there is always a subtle warm _selfless_ sensation of gratitude in the body, now, underneath the balance-book of the mind's reckonings. No matter how bad it gets, there is always something to feel gratitude for.

* Melting thoughtlessly into the strangeness and actuality of ordinary phenomena. The entire universe exists in the space between ordinary things, vast and strange. To see it is thoughtless awe, to travel across it, adventure, to express it well, art.

* And all this, of all places, in the post-office! while doing the washing up! even watching the news!

Remember, there is no need to _try_ to do these things, to be happier or more truthful or what-have-you. Trying, like 'I can't do this', is a trick of the ego, to make you think.

## THE FUCKER: SEX WILL CRUCIFY YOU

Unless you are in contact with the delighted love-feeling that contact with the opposite sex naturally bubbles up (regardless of whether you are in a relationship or not), or able to feel and let slip away sexual frustration or its money-business-victory substitutes, you'll be a slave to murderous sex thoughts and to porn, both subtle and obvious. The anxieties and agonies of the restless sexmind lead to a catastrophic lack of discrimination — unable to tell the gentlemen from the beast, prey to the restless, the cold or the violent — and to an equally catastrophic lack of presence — unable to talk to that nice girl in the sandwich shop, unable to be simple and affectionate, unable to make [ten thousand years of godlove](https://expressiveegg.org/2017/04/18/ten-thousand-years-of-godlove/).

Sexual frustration is the driving force of the world, particularly the male world, which was created, in large part, to subjugate woman and reduce her to the status of sexual vassal. That woman now also fights to reach the top of the degrading and sordid male system, that she has successfully claimed the right to be just as sexually frustrated as successful man, just as acquisitive and cold-hearted, [doesn't make it any less inhuman, it just degrades her too](https://expressiveegg.org/2017/03/29/the-tao-of-gender/).

Love — sexual love, partnership and romance — is in a complete state. How are we supposed to create a society worth living in if we can't create a decent relationship? How are we supposed to bring genuine peace, warmth, understanding or togetherness to our social lives if our private lives are scarred with compromise, casual dishonesty, sadism, masochism, desperation, coldness or the anguish of a loveless partnership? Is it really such a radical suggestion to deal with the problematic world we actually live in before turning to 'society?'

### practice

* Using the modern world to experience the awareness that precedes reflexive glances towards stimulus-response tits and arses. Nothing wrong with enjoying a beautiful body, or gesture or dress or whatever — it's the _automatic_ grasping (isolating) eye you must master. Feel the sensation of this automatic grasp — it is not pleasant.

* Giving up porn, in all its forms. _This is absolutely critical._ Porn leads to dulling of general awareness (or 'affect'; i.e. consciousness), lower libido and greater anxiety; it is highly addictive and leads to the same problems as drug or video-game addiction. Give up masturbating too. BUT: do _not_ fight the urge; don't try and resist — this just makes it harder. Instead, feel the restless one. Locate the edgy need in your body, and, if you must wank over an idea or image, do it as consciously as you possibly can; notice the crude, searing effect it has on your heart; completely inhabit the hollow, irritable, metallic shell-self that (usually) results. Face it, full on. The same applies to all other drug-addictions (fame-hunting, wealth-gathering, belly-cramming, booze-gutting, mind-filling, etc); if you're going to do them, fine; but watch how it _really_ makes you feel, _as_ it happens.

* Scriptlessly, wantlessly facing the opposite sex in spontaneous ungrasping unknowing, and self-surrendered enjoyment of his or her simple presence. Ego only knows _want_ (she's hot / he's hot) or _don't want_ (too old, too fat, too ugly, too poor). It doesn't know the simple pleasure of physical affection.

* Consciousness perceives quality, or lack of it, and enjoys that; not whether an interaction might lead to getting laid (or getting married, or getting promoted). Practice long courtships, wooing and slow-winning — not just because they are a means to master self and to see if he is really that into you, but because they are delightful.

* (And never stop winning her. She is _never_ won.)

* Physically loving your partner when 'you' don't feel like it. It's _ego_ that gets bored and irritable, that starts to find the other ugly, that 'isn't in the mood' (or is moody). If you are with someone you love, then love her, physically, sweetly, creatively — every day; every hour if you can. This will weaken the genetic fuck-and-flee instinct and subvert the surreptitious emotionality that ego uses to dominate your life. It's also enjoyable.

* Letting go of the constant clench of wanting. Practice loving physically (rather than mentally or emotionally) by experiencing the lights-on actuality of the act. Conscious sex (also, cheesily — but accurately — called '[love-making](http://www.barrylong.org/statements/makinglove.shtml)') is not primarily a mind-genital act. It is physical; the whole body is involved. If you cannot enjoy the whole body, the whole smell, touch taste and apperceptive pleasure, you'll never know how pleasurable sex can be.

* Refuse all sex that is loveless. This is particularly important for woman. Do not let a man inside you until you have tested his conscious presence — is he powerful enough to overcome his inner cock when, after a few weeks, fuckery starts to reassert itself?

* Note that when you turn from ordinary fucksex to love-making the ordinary momentum of the self panics or wilts. You may find you are not aroused. It may seem silly. You might get emotional. More tricks of the ego — the guardian at the gate of paradise. Push on through to the simple body and the simple truth. Express this — tell your partner how they made you happy today (or whatever — anything I suggest here will sound cheesy). Nothing, but _nothing_, is as intensely, wildly, arousing as love.

* The more you love — physically, fully, foolishly — the more you come to experience a state where ego is secondary, or silent; the less you **stare** — gripped, addled, fixed — and the more confident, present, sincere and gentle your simple, bright **gaze** becomes. Very desirable.

## VIOLENCE: NOT WHAT YOU EXPECT

…is built on sexual frustration (the connected warmth of love-making is absolutely without violence), and depends on restlessness and, above all, _expectation_. In civilisation violence must be continually suppressed — because we are continually afraid of expressing the psychotic pent-up cause of it. This stuffed-down anger and irritation, which eats you away from the inside, comes out in the weirdest and most indirect ways, often flaring up into veritable orgies of rage 'for no good reason'. Ever had a ludicrous argument over nothing at all? _Pure_ ego.

### practice

* Dealing with your anger — men particularly. Not by the count-to-ten effort of self-control, but by feeling your restlessness in your body.

* _Looking for the expectation that causes or caused the anger._ If you can find whatever it was you are or were expecting or hoping for that caused the anger, and be honest about it (first of all to yourself, then to others), you're halfway to deflating it.

* Confessing. First to yourself and then, if possible, to the other that you are emotional. That's the other half.

* Looking for the over-excitement which caused your wound-up hyper-irritable state. Playing a first-person shooter, watching a violent or titillating movie, thinking through a little revenge fantasy, slapping a few mind-bums — all these things can speed your self up, and make it hyper-sensitive to frustration or criticism. Feel it. Be that which feels.

* Remind yourself, when angry (or afraid or depressed), that no situation is so bad that you cannot laugh at it, or find it interesting or somehow softly, strangely right.

* Facing other people's violence is a slightly different matter, but it too requires instant discernment, the subtle art of calm, watchful prisoner's defiance, sensitive threat-awareness or the death-fearless chucking of all chips to the wind in order to defend someone else. You can practice most of these in the arenas of cruelty that civilisation currently offers; the office, the factory and the family.

* If you are lost in anger, if you cannot let go of it instantly, it is all your fault. No matter what it is you are angry about, no matter how justified you are, no matter how badly you've been treated or how insensitive someone else is being… _you are to blame_. You only have the right to point the finger when you are present.

* For anger is not the same as noble ire. Consciously fighting for what is right, consciously seeing the truth and speaking it, or acting on it, confronting the violent resistance of someone else; this is not a problem, quite the reverse. Running from facing up to the fight into pacifism or 'being nice' is cowardice (indeed a cowardice that the most famous 'pacifist' of our times, Mohandas Gandhi, repeatedly denounced).

* Note also that it is futile to the point of self-indulgence to give your consciousness to those completely in the grip of the Unhappy Supermind. It will create fury in them and a thirst to destroy you.

## FEAR AND ANXIETY: EGO REVEALED

Fear and anxiety are the very nature of ego, or [self-in-charge](https://expressiveegg.org/2017/01/01/duck-rabbit-duality-paradox-origins-civilisation/). Ego is _constantly_ afraid — a sensation of restless tension that converts into anger, craving, guilt, self-indulgent misery, neediness and depression. The craven cowardliness of ego is rarely seen for what it is, as civilisation offers so many ways to mask it — all of the useless stimulants and tinpot offices and accolades of world, not to mention all the opportunities it affords us for having an indirect relation with our fellows; all serve to suppress, redirect and mask our underlying terror of life. We can hide our fear behind money power, behind information technology, behind bureaucratic formality, behind insignia and rank, behind racial or sexual bigotry, behind education and professional power or behind intoxication. We can and we do — until we can't any more, and then fear reveals itself as naked violence.

Remember that ego starts and ends in emotion. The inability to stop thinking (worrying, wanking, etc) is the result of the self-informed emotional impostor that lives in the pit of the chest, which lashes out when threatened or hurt. Dealing with this beast (a literal devil) cannot be done in the mind. Only conscious presence and action purifies.

### practice

* Seeking out situations in which you cannot use or rely on self; situations that self fears or that it really 'doesn't like' — aloneness, poverty, unscripted theatre, nature, extreme boredom and the company of the young, the dying and the mad are the classics, but everyone has their own private self-hell which, sooner or later, must be faced. Better to do it in your own way, now, than to be propelled into it by civ-pop.

* Living without the props that self uses to big itself up. Again this varies from self to self. For some it means technology, for some money-power, for some the ego-inflating pride of being cultured or well-travelled or 'enlightened'; for some it means routine, always knowing what the day is going to bring, for some it means an important meaningful job, for some it means being right, being one of the good-guys, and for some it simply means being special, important in some fabulous way (even if only in the mind).

* Living without the drugs that you use to suppress fear and the pain of fear. Giving up porn, video games, box-sets, dope, alcohol, shopping, anti-depressants, internet fame-mongers and 'fun'. After another day of stupefying mundanity, kow-towing to a flabby manager (who's just _such_ a nice guy) and struggling across town on the tube who wants to sit with the naked self or seek out pleasures which dignify it? Few; and who can blame them? A large part of addiction-mastery is giving up the shitty work that makes it so necessary — but even while working it isn't just possible to free yourself, it necessary.

* Crossing taste-boundaries. Seek out unusual experiences. This does not mean travelling across Afghanistan on a llama (although it might!); it could just be volunteering at a local care home to chat with the old people, or buying a dead engine and taking it apart, or reading up about mushrooms or camp dancing in the stairwell or learning to love [Bach](https://expressiveegg.org/2017/05/18/bach-and-partridge/). The point is not so much 'novel experience' or even 'learning' but _widening_. Self loves nothing better than what it knows it loves — overcome its fears by overcoming its knowledge.

* Mastering your craft. If you don't know what you love, try many things (particularly important for [the young](https://expressiveegg.org/2019/07/23/words-of-truth-for-young-people/)). If you do know what you love, learn to do it superbly. Open yourself to the finest work and then work, every day, to create it yourself.

* Facing your fears and phobias. Scared of heights? Needles? Wasps? Scrapy sounds in the night? Find a way to face them. You might never overcome the sweaty-anxiety of your personal hell; but you can find that your built-in reflex terror (your 'button') is not you. You can know that, if push-comes-to-shove, you can face room 101.

* (Note though that ego can actually feed off the trembles of fear-facing: observation of the mind is of the essence, not having an exciting time).

* Resisting subjugation and refusing constraint. It is impossible to master self while that self is owned by [a system built upon and continually feeding from it](https://expressiveegg.org/portfolio/33-myths-of-the-system/). Complete freedom from this world is impossible, but the dignity, anxiety and suppressed unlife engendered by continually '[living](https://expressiveegg.org/2016/08/31/5-things-to-do-before-you-die/)' and [working](https://expressiveegg.org/2016/09/18/freedom-from-work/) in a [deathless unworld](https://expressiveegg.org/2016/08/31/5-things-to-do-before-you-die/) will not magically vanish with half an hour's meditation. Self-mastery is as 'political' as it is 'spiritual' (and revolution is as 'spiritual' as it is 'political').

* Improvised theatre. One of the most terrifying and useful things you can possibly do. Scarier that space-travel, but far more enjoyable. In a good impro class (or, better, by forming your own group and learning yourselves) you learn a) how to fail over and over again, and enjoy it b) how to go into uncertainty big (rather than the pewling excuse-making hunched over 'sorry this is going to be shit' stance of the sure-to-fail) and to trust it c) how to give to the scene (rather than grasp for the limelight) d) how to pay soft attention e) how to be obvious and clear f) how to let go of ego g) how to let go of plans as they fly across your face h) how to let go of failure (_'oh no, I screwed up!'_) and pride (_'wow, I'm really getting good at this!'_) and re-enter the collective flux i) how to experience outrageous collective joy.

* Feeling it. Anxious? Feel it — where is it in the body? Tightness in the chest? Hollowness in the craw? Pressure on the back of the head? Wanty-wanty must-must-have tightness across the thorax? Feel it. In feeling it, you are not it, and it has no power over you. Likewise, in feeling the feeling of what you most want (now), without the image or name of it, endless wanting evaporates into having the only thing you ever really can have.

* Seeing it. Talking too much? Telling the same dull story? Holding court? Bigging yourself up? Lying? Deceitfully wheedling attention? Dishonestly covering your tracks? Losing your blessed mojo? See it. Trying to change what you (and everyone else) don't like about yourself is to insanely split yourself into three — 1) the self that is (say) lying, 2) the self that doesn't want to lie and 3) the ideal truthful self that self 2) is trying to turn self 1) into. This is the quintessence of futility. The only thing that really changes people is consciousness (seeing the problem) and…

* Action. Ego prefers to fret and talk over problems that to do anything about them. It is terrified of acting, of entering the room or of leaving it. Of course mindless activity doesn't help, and meaningful action, in this sense, can mean saying what is on your mind to your mother, your lover or your boss. If you cannot act — accept completely and let the 'ifs' come and go; then, when you have to act, let the body act. There is no need to make a decision, or to try, or to go over and over your plans — these will just take you away from the presence you need to seize the moment to act when it does come. And it will. It always does.

## DESPAIR AND THE BALEFUL ONE

World despair is another luxury the self indulges in. It's analogous to the ghastly mask of grim woe that stares back at you in the bathroom mirror when you're splitting up with your lover or in the middle of a profound self-pity wank. That's not to say that death of loved ones, loss of the past and the ongoing destruction of everything that is beautiful on earth isn't cause for heart-splitting sorrow, but sorrow is absolutely different to despair. Sorrow breaks you open, stills you to your core and lets in the strange-awesome light of life — even as all is lost. Despair is self-indulgence.

When self is mastered there remains an urge to subvert the world, a joyous, chaotic, anarchic and utterly carefree instinct to confront the horror of infinite loss with the mad, free, brilliance of life as it is — in art, in revolution, in improvised theatre, in sweet collectivisation or in naked gardening. Or there remains the diamond stillness of miraculous composure and outflowing presence into the mad body of the world. Or there remains something that you long-ago forget you were; the pre-civilised I that beats wildly beneath the endless futile worry of endless futile struggle. This perfect truth, this strange subversion is not going to 'save the world' — perish the thought! It is, rather, what remains when the world is seen for what is… and what remains will — as everyone who steps back from the time-space self knows — remain forever. This extraordinary response to catastrophe, far more valuable than marches and protests, is the candlelight of genuine culture, which we have no choice but to carry through darkness, even unto death herself.

Self mastery is the first step to living well, in any society, whether it is [rapidly collapsing, or slowly collapsing](https://expressiveegg.org/2016/08/27/end-of-the-world/), or not collapsing at all. We all know this, but the mission to get to the conscious root of our own experience has been hijacked by the new-age self-improvement movement which is more interested in fostering specialness and functioning more efficiently in the office than in genuine self-mastery, which always leads to subversion, refusal and sacrifice and, consequently, to _rebellion_.

### practice

* Welcoming emotional pain. The weak, the selfish and the stupid run from inner pain, will do _anything_ to shut it out, escape from it or stifle it with drugs, excitement or blind activity; they will moan to others — not to share, confess or learn from another but rather to offload, suck attention, justify and make excuses, always excuses; or they will whine to themselves, over and over again, how _unfair_ it is, how _shit_ life is, what a _loser_ I am.

* The real man, the real woman, knows that such pain is not just a part of life, but the voice of the whole. They _welcome_ the pain — not fatalistically, not passively (for there is almost certainly conscious action to be taken) but psychologically accept, face up to what is, and seek the truth of it; not just 'the lesson' of the pain, but the profound _reality_ of it and, at the end of the road, the awesome, shattering heartbreak of it, which, in truth, is actually **selfbreak**, the splinter that lets the light in, and the light is… the last thing you dare look at.

* The more that pain is accepted in this way, the more sensitive you become to it, and the less often it has to shout — just the slightest whisper — which we call **conscience** — is enough to know that the bellymind is speaking, asking you to change direction.

**See also: [Words of Truth for Young People](https://expressiveegg.org/2019/07/23/words-of-truth-for-young-people/).**

## RESPONSES:

**I normally love what you write. I often find conclusions I had come to myself, but to have those thoughts and feelings encapsulated so well is extremely satisfying. You are not the bloody Vicar of Christ though! Preaching at people like this doesn&#8217;t do anything except make them feel guilty and ashamed.**

You are most grateful to me for pointing out what you already knew. This is, as you say, '_satisfying_'. Not very _useful_ though. It's through discovering what you _don't_ already know that really changes men and women. Of course this cannot be described. It's something you do, or live, not something you read and intellectually recognise. Of course for the ego reading of how to do and live in this way is not 'satisfying'. The ego will complain that it is being preached at, that it feels 'guilty and ashamed,' that the author of the advice is a pompous nob in need of an editor, etc.

Action is the solution here though, rather than blaming the advice for existing. In short; up your game.

**Self-mastery is just effort and denial. Or naval-gazing mumbo jumbo.**

No, self _control_ is effort and denial. Self control (as Krishnamurti spent his life saying) is one part of the self — an idea or emotion — controlling the rest — which is, yes, frustrating, difficult, suppression. Self mastery is the whole context, or unself, in charge. The difference between the two is both phenomenally subtle and unbelievably vast.

As for the mumbo-jumbo — there's a lot of that about. NLP, self-help, guides on finding new chakras, the latest polemic from a sage on a stage… But dismissing all self-enquiry because of such people is as stupid as dismissing anarchism because a few kids threw a few bombs, or dismissing men because so many of them are running around like cocks with their chickens cut off, or dismissing all science because of rabid maniacs like Richard Dawkins.

**My situation is different. I can handle these problems.**

Your own particular practice depends less on any one of the tips mentioned here — which require months and years of constant practice — as on facing your own particular selfmare. You might find it easy, for example to walk through a corpse-strewn battlefield, but play clunkily with children, have secret sexual shames, and talk down to your mother. Or you might be a yogic master, able to suck water up your anus, but be afraid of any kind of definite judgement, uncomfortable around the working class and weird about money.

The super-intimate skills of self-mastery are not acquired by any specific effort, psychology, magic, education, community-action or self-knowledge, but by actively seeking out criticism, uncertainty and the experience of unself, and by practising, again and again, letting go of self, feeling out and allowing subtle internal pain, listening to its message and then courageously, selflessly acting before the manifold opportunities normal life offers to lose your presence, break down, throw a wobbler or behave like a dick.

The above guide is just that, a guide. It is not, nor could possibly ever be, a template for your own experience, which contains [your own perfectly unique selfmare](https://expressiveegg.org/2017/01/27/history-philosophy/).

**Do you have children? All very easy if you are single! Having kids is a unique challenge.**

Having or not having children has nothing to do with self-mastery. In fact if they challenge practice differently or make it more difficult then this makes them _less_ of a barrier to self-mastery. The more anxiety, stress, hurt and annoyance something or someone causes you, the greater the opportunity to face down your self.

All to the good; mastering your moods, irritations, selfish fears (meaning the smothering controlling anxieties some mothers have and mask as 'care') or blithe hardnessnes of heart is essential if you are to raise a child that doesn't spend her whole life trying to even perceive the vibe-conditioning your self warped it with as a bairn — let alone deal with it.

**You over-emphasise sex. Sex is not so important.**

In theory, it's true that relationship problems and sex are not so important to life in the world, collapsing or otherwise. In practice just about everyone yearns for romantic love, has or has had chronic relationship problems, are absolutely crippled with obsession or encoldened by boredom and the experience of 'normality'. Solving all this is clearly a vital part of life. But I am making a fundamental distinction between sex (good _or_ bad) and love-making. Here is the difference:

**Sex** is a restless, reality-excluding mental-emotional tight-grip focus on a self-created image: no different to masturbation and porn — there just happens to be someone else there.

Bestial sex-want only creates problems. It is violent, desperate, quickly bored but _never_ satisfied and can only be controlled by cold brain-clamp. Sex with another (and constant sex-fantasy) creates distance between you, spike, irritation, annoyance, all sexual problems and many (perhaps even most) social problems.

**Making love** (or outrageously conscious sex-fusion or whatever you want to call it), on the other hand, is an experience of total sensory floodout — the 99.9% of the sense data normally excluded by the mental-emotional self is perfectly allowed in a state of self-annihilating devotion and near hideous strange-delight. This is no different from the full-scale wide-attention life of über-woo which surrounds it.

Transdimensional animal love-making (and constant woo) creates liquid ease, lack of cling and creative amazement all day — but to reach the garden you have to be able to know how to give up your self — and you have to want to — which is impossible while men and women are addicted to their silly plans and schemes.

Love-making, in this sense, is not a refuge for the self — and it lasts all day.

(I've made the distinction clearer here than it sometimes is in practice though. The restless sex-brain can interpose itself into love-making and present lovers can bring each other out of the virtual world. Once again, explained fully [here](https://expressiveegg.org/portfolio/apocalypedia/).)

There are, of course, other ways to overcome self than making-love or really being with the opposite sex — there's the whole of your life, for a start! — but sex and love-making are a huge part of men and women's lives, and must be addressed if self is to be mastered (or allowed to be swept sweetly away).

**What about community? We should focus on our external lives.**

Community is a good testing ground for self-mastery. All your insanities and stupidities will out if you have a direct relationship with your fellows. Few of us do have such a large direct-relationship group though — but nearly all of us have romantic relationships — the community of two — and all of us have a self, which is where _all_ problems begin.

It is, amongst the cosy world of bourgeois doomers (such as most of Dark Mountain), a _massive_ red-herring to suggest that one _first_ look externally, to 'community' or even to 'nature' to solve the problems of self. It is not about where you look, but who, at any particular moment, is doing the looking. When your wife turns her face to the wall and you feel the whole dread universe between you, or when a writhing monster has taken possession of your child and is pushing all your buttons, or when you are walking through a forest and are not mad overwhelmed by the weird vibrating god-beauty of it, or when you just cannot stop wanting, worrying or yibber-yabbering inside about something someone said about you — the solution, before external action, is an _inward_ release. Without this release all action leads to more problems.

**But this has all been said before. There are many paths of 'gentle wisdom'.**

There are, of course, many penetrating spiritual teachings of pedigree — but it is often difficult to apply their lack of specific guidance — on dealing with pointless work, 'causeless' emotion, sexual apathy or desperation, raising children in a virtual environment and wotnot — to one's actual, chaotic and modern lifestyle (for reasons I outline in the second, Eastern, part of my [history of philosophy](https://expressiveegg.org/2017/01/27/history-philosophy/)). We do not live on Zen mountains and in Hindu fields. This is not a problem with the teachings of course, as they still get so finely to the point (particularly Advaita, Zen and Tao) but a great deal of dressing up and being special surrounds spiritual tradition and the wily western mind is, if nothing else, a genius at subtly missing the point.

Nothing quite hits the sweet spot (or puts its finger on the wurm) like the truth fresh, which is why contemporary 'paths' (if you can find one that is true) are more direct. This is why most great teachers are not revered until they're safely dead.

Incidentally, I'm not enlightened, me — I still have moodies, confusions, irrational fears and stupid desires — and, in some ways, I'm not even sure it's possible to be. My own approach to solving problems — with art, society, relationship and self — is to head towards _an unrealistic standard of total perfection_ (masterpiece, utopia, endless romance and enlightenment)  while accepting that I might never reach it, it might not even exist and that, if 'enlightenment' is to mean anything it is 'okayness with endarkenment'. This approach has worked extraordinarily well for me and has yielded insights which I am compelled to share and to do artistic justice to. Hence this post and more besides.

As far as all this self-mastery malarky goes my own 'unrealistic standard of total perfection' has come from the teachings of the [Thomasine Jesus](https://expressiveegg.org/2016/08/25/christian-nazarene/), from the [Tao te Ching](https://en.wikipedia.org/wiki/Tao_Te_Ching) ([Chen translation](http://www.paragonhouse.com/Tao-Te-Ching-The-A-New-Translation-with-Commentary.html)), from heretical medieval Christian mysticism ([Tomas á Kempis](https://en.wikipedia.org/wiki/Thomas_%C3%A0_Kempis), [Meister Eckhart](https://en.wikipedia.org/wiki/Meister_Eckhart) and [Jacob Böhme](https://en.wikipedia.org/wiki/Jakob_B%C3%B6hme) — all synthesised marvellously in Aldous Huxley's [Perennial Philosophy](https://archive.org/stream/perennialphilosp035505mbp/perennialphilosp035505mbp_djvu.txt)), from various bits of Buddhism ([Theravada](https://en.wikipedia.org/wiki/Theravada) and [Zen](https://www.amazon.co.uk/d/Books/Essential-Dogen-Writings-Great-Master/1611800412/ref=sr_1_10?ie=UTF8&amp;qid=1485280622&amp;sr=8-10&amp;keywords=Shobogenzo)) and ([all men you'll notice](https://expressiveegg.org/2017/03/29/the-tao-of-gender/)) from the teachings of [George Gurdjieff](https://en.wikipedia.org/wiki/George_Gurdjieff), [Ramana Maharshi](https://en.wikipedia.org/wiki/Ramana_Maharshi), [Jiddu Krishnamurti](http://www.jkrishnamurti.org/default.php) and, the teacher I learnt most from — and in these matters far, _far_ superior to me — [Barry Long](https://expressiveegg.org/2019/08/01/its-barry-long-day/) (he coined 'the baleful one,' and was the first teacher ever to make such an intelligent distinction between feeling and emotion, and his writings on love-making are phenomenal) all of whom I recommend. Again, I can't say for sure if they were 'enlightened', but I do know they all represent a direction worth heading in.

Finally, talking of 'marvellous syntheses', my book, [The Apocalypedia](https://expressiveegg.org/portfolio/apocalypedia/), is an attempt to summarise and synthesise all of this with… well, with everything else.

---

**this text originally appeared at [](https://expressiveegg.org/2016/12/18/self-mastery-empathy-enhancement-consciousness-softening/)**

~
""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

def paragraphize(iterable):
	for paragraph in iterable:
		if paragraph.startswith("<"): yield paragraph
		else: yield "<p>" + paragraph + "</p>\n"

if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	config['sections'] = [paragraphs]

	main(config)

