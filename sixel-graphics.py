import _builder
import _config
from _template import tag
import sys

import markdumb

text = """
# Sixel Graphics

<<<Under Construction>>>

## What are [sixel](https://en.wikipedia.org/wiki/Sixel) graphics?

👉 Did you know:

You can display actual bitmap images on the actual console in selected terminal emulators.

[https://en.wikipedia.org/wiki/VT340](https://en.wikipedia.org/wiki/VT340)

Observe:

![graphical 'this is fine' meme in an xterm](assets/sixel-graphics/87f9a824b2d4d037.png)

## How can I display sixel graphics?

### First you need a sixel-capable terminal emulator

[XTerm](https://invisible-island.net/xterm/) supports this, but needs a little nudging to enable:
```
$ xterm -xrm "XTerm*decTerminalID: vt340" -xrm "XTerm*numColorRegisters: 256"
```
But do you a favor and run the following:
```
$ echo "XTerm*decTerminalID: vt340" >> $HOME/.Xresources
$ echo "XTerm*numColorRegisters: 256" >> $HOME/.Xresources
$ echo "XTerm*disallowedWindowOps: 1,2,3,4,5,6,7,8,9,11,13,19,20,21,GetSelection,SetSelection,SetWinLines,SetXprop" >> $HOME/.Xresources
$ xrdb $HOME/.Xresources
```
And all your XTerm sessions will have 256-color sixel graphics enabled by default.

(The third line sets up some exceptions so we're allowed to query XTerm about its active screen size in pixels, from which we can then calculate the character cell size, which is kind of important to know when you want to align sixel graphics with text. Other terminal emulators already allow this by default.)

The only _other_ sixel-capable X terminal emulator offered by _Debian Buster_ is [mlterm](http://mlterm.sourceforge.net). No special options are needed to enable sixel graphics.

See the [libsixel home page](https://saitoha.github.io/libsixel/) for other _sixel-capable_ terminal emulators, including ones for the framebuffer console.

### Then you need some sixel graphics

* Install `libsixel-bin` and use its `img2sixel` program to display most common image formats.
* [ImageMagick](https://imagemagick.org/) (but **not** [GraphicsMagick](http://www.graphicsmagick.org/)!) can convert to and from sixel format.
* I keep some [sixel demo programs](https://gitlab.com/Matrixcoffee/sixel-experiments) at [my GitLab](https://gitlab.com/Matrixcoffee).

## Gallery

`population.awk`:

![Screenshot of population.awk](assets/sixel-graphics/Screenshot from 2020-09-05 23-03-33.png)

`horigraph.awk`:

![Screenshot of horigraph.awk](assets/sixel-graphics/Screenshot from 2020-09-06 10-03-03.png)

`population-bitmap.awk` (v1):

![Screenshot of population-bitmap.awk](assets/sixel-graphics/Screenshot from 2020-09-06 11-56-07.png)

`population-bitmap.awk` (v2):

![Screenshot of population-bitmap.awk](assets/sixel-graphics/Screenshot from 2020-09-06 13-32-58.png)

`bifur-bitmap.awk`:

![Screenshot of bifur-bitmap.awk](assets/sixel-graphics/Screenshot from 2020-09-06 16-53-04.png)

`text-test.sixel`:

![Screenshot of text-test.sixel](assets/sixel-graphics/Screenshot from 2020-09-06 20-25-22.png)

`mandelbrot-bitmap.awk` showing the full mandelbrot set:

![mandelbrot-bitmap.awk showing the full mandelbrot set](assets/sixel-graphics/Screenshot 3 mandelbrot-bitmap.awk.png)

`mandelbrot-bitmap.awk` - usage - zooming in:

![mandelbrot-bitmap.awk - usage - zooming in](assets/sixel-graphics/Screenshot 2 mandelbrot-bitmap.awk.png)

mandelbrot-bitmap.awk showing a zoomed-in view of the mandelbrot set:

![mandelbrot-bitmap.awk showing a zoomed-in view of the mandelbrot set](assets/sixel-graphics/Screenshot from 2020-09-08 01-51-27.png)

sixkcd.bash black-on-white:

![Screenshot of sixkcd.bash showing xkcd comic 2154, black-on-white](assets/sixel-graphics/Screenshot 5 sixkcd.bash.png)

sixkcd.bash white-on-black:

![Screenshot of sixkcd.bash showing xkcd comic 2154, white-on-black](assets/sixel-graphics/Screenshot 4 sixkcd.bash.png)

## Resources

* [Sixel](https://en.wikipedia.org/wiki/Sixel) at Wikipedia
* [Libsixel](https://saitoha.github.io/libsixel/) ([github](https://github.com/libsixel/libsixel)): Explanations, related projects, screenshots, examples. Its helper programs such as `img2sixel` are available from _Debian Buster_ package `libsixel-bin`. It also provides [Python](https://www.python.org) bindings, but sadly these aren't packaged for _Buster_.
* [All About SIXELs](https://www.digiater.nl/openvms/decus/vax90b1/krypton-nasa/all-about-sixels.text) by Chris F. Chiesa.
* [VT330/340 Programmer Reference Manual Volume 2 Chapter 14: Sixel Graphics](https://www.vt100.net/docs/vt3xx-gp/chapter14.html)
* "[Jexer](https://jexer.sourceforge.io), a text windowing system that resembles Turbo Vision. The first and only text windowing system that supports mixed text and images to Xterm via sixel. Runs on Linux, Mac, and Windows. Written in Java. Feature complete."

## Supplemental Resources

* [XTerm Control Sequences](https://invisible-island.net/xterm/ctlseqs/ctlseqs.html)
* [ANSI escape code](https://en.wikipedia.org/wiki/ANSI_escape_code) at Wikipedia
* [POSIX.1-2017 Chapter 11: General Terminal Interface](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap11.html)

~
""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

def gen_construction():
	s_construction = tag("section", attrs={"class": "construction"})
	s_construction.append(	tag("p").append(tag("picture").append(
					tag("source", media="(min-width: 465px)", srcset="assets/cuc/BaBajaCanyon9450construct4.gif"),
					tag("source", media="(min-width: 170px)", srcset="assets/cuc/kikingsnakesmoonMealInABowlneonconstruction.gif"),
					tag("img", src="assets/cuc/BaBajaTrails8723construct.gif", alt="WARNING: this page is under construction"))))
	return s_construction

def paragraphize(iterable):
	import codecs
	for paragraph in iterable:
		print(codecs.encode(paragraph.encode("utf-8"), "hex"), file=sys.stderr)
		if paragraph == "&lt;&lt;&lt;Under Construction&gt;&gt;&gt;": yield gen_construction()
		elif paragraph.startswith("<"): yield paragraph
		else: yield "<p>" + paragraph + "</p>\n"

if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	config['sections'] = [paragraphs]

	main(config)
