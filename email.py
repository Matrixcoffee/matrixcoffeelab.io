import _builder
import _config
import sys

import markdumb

text = """
# Why I choose to Not Have a Public E-Mail Address

**STATUS: WIP**

1. No control over who can reach me. As soon as anyone even *guesses* my address correctly, they can message me against my will, *keep* messaging me against my will, in any quantity they like, and there's nothing I can do to stop it. I, or a service on my behalf, must receive and process this junk, even if I'm filtering it out later in even more processing.

2. Outside of geek circles, HTML mail is the standard. I don't mean some text lightly sprinkled with HTML tags. I mean gobs and gobs of CSSHTMLJS vomit with the actual message scattered about in small crumbs so don't even *try* reading the source code.

   The cool new trend is to send a multipart/alternative body with the text part either empty or saying:

   > your client doesn't support HTML

   or, if you're "lucky":

   > Your client doesn't support HTML. Visit this link to view. http://track.mailspam.biz/?id=asdfghjkl

   Even if my client is capable of rendering HTML, but is set to prefer plain text, I see this non-content instead of the intended message.

3. Tracking. An e-mail address serves as your unique identifier. That's a bad thing in an online world where companies predate on your private information. To mitigate this, [Use Unique Email Addresses](https://musings.tychi.me/the-case-for-unique-email-addresses).

4. Let's type up the message in Word and send that as an attachment. Who needs body text, plain *or* HTML?

5. Privacy. I understand e-mail is not private. You understand e-mail is not private. Most people don't. They'll send me sensitive mails, or ask me to send sensitive information. Actual thing that happened: "Hi, can you e-mail me a scan of your passport? Love, your boss."

6. More privacy. I don't use GMail. Everybody else does. That means my messages are scanned and processed by Google, which is probably building a profile on me. This despite the fact that I never agreed to any TOS, nor made any other kind of agreement with Google. Not even implicitly. Apparently total strangers, who I may never have exchanged words with before, can agree to this on my behalf. Bizarre.

   One cannot assume sending mail to a non-gmail.com domain is safe: often these are just forwarders with a GMail account as its final destination.

7. Actually, privacy. Yes, privacy. Why the fuck can't *all* my day-to-day communications be private? You're telling me to use e-mail, but to not expect any privacy out of it? Fuck you!

8. `noreply@*`. Because we want to talk *at* you, not *with* you. Granted, if I were the sort of person to commit the atrocities listed here, I wouldn't want to hear back from my victims either.

9. Entanglement with the web. People assume you receive mail on the same computer you use to browse the web.

   a. "Here, have this 500-character confirmation url made of hard-to-read random characters, which you can just click, oh haha we mean copy&paste, oh haha we mean manually type into the other computer which is set up to browse the web securely and privately. (Because the web is utter crap and to have some semblance of security, it should be used only from an entirely separate, dedicated computer. But this text is about e-mail, not the web.)

   b. "Here, have this newsletter with only the headlines and short teaser blurbs. Just click each section to read." What the fuck shit is this? I have to be *online* now to read e-mail I already downloaded? Also, your links contain tracking IDs. Also, I can't click because separate computers, remember?

      Maybe if you sent your newsletter in plain text and/or clean HTML, it would actually fit in an e-mail. [Narrator: It would fit very comfortably indeed.] Ah, but that way you can't measure "engagement". Imagine having to estimate engagement based on actual human beings replying with actual messages. Fortunately, somebody invented that noreply address we talked about earlier.

All in all, it's much easier to just tell people I don't have e-mail. (And give up on the idea of newsletters via e-mail.)

To be continued...

## Resources

### Email bad

* [How Google and Microsoft made E-mail Unreliable](https://battlepenguin.com/tech/how-google-and-microsoft-made-email-unreliable/)
* [@cypnk@mastodon.social](https://mastodon.social/@cypnk) on [problems with spam on his own server/domain](https://mastodon.social/@cypnk/104609070938177904). A thread.
* Getting caught in spam filters: [(1)](https://toot.cafe/web/statuses/105237209289725308)
* [13 More News Articles Which Prove How Horrible E-mail Really Is](https://mensago.org/13-more-news-articles-which-prove-how-horrible-e-mail-really-is/)

### Replacing email

* [https://mensago.org/](https://mensago.org/)

~
""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

def paragraphize(iterable):
	for paragraph in iterable:
		if paragraph.startswith("<"): yield paragraph
		else: yield "<p>" + paragraph + "</p>\n"

if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	config['sections'] = [paragraphs]

	main(config)
