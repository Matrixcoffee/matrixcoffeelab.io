from _template import tag, apply_template
import _config
import sys

import markdumb

text = """
# Random Links

(In random order)

* [Self-Mastery: Empathy Enhancement and Consciousness Softening](https://expressiveegg.org/2016/12/18/self-mastery-empathy-enhancement-consciousness-softening/) - All problems are caused by **the self-informed self**, which is to say by the mechanism of the thinking-emoting mechanism I normally call 'I' -- the **self** -- receiving inspiration, intelligence and instinct not from the source of these things -- the _unselfish_ **consciousness** and **context** --- but _from_ the self. When mind tells mind what's right or what to do, when emotion tells emotion what I want or don't want, the result is an endless self-replicating nightmare of addiction, frustration, fear, ignorance, despair and madness. - by [Darren Allen](https://expressiveegg.org/)

~
""".strip()


def main(config, sections):
	for line in apply_template(config, sections):
		print(line)


def paragraphize(iterable):
	for paragraph in iterable:
		if isinstance(paragraph, str) and not paragraph.startswith("<"):
			yield tag("p", flow=True).append(paragraph)
		else: yield paragraph
		yield ""


if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	main(config, paragraphs)
