import _builder
from _builder import html
import _config
import sys

text = """
# So You Want to Hire a Coffee

*This page is being rebuilt and, until done, serves as a dumping ground for ideas and concepts.*

> [Scientific Taylorism](https://en.wikipedia.org/wiki/Scientific_management)
> was the dominant theory of industrial management. It explicitly treated
> workers as machines whose performance should be optimized with intensive
> management controls.

https://meaningness.com/renegotiating-self-society

> Work in the industrial economy felt dehumanizing. Extreme division
> of labor made most people tiny, interchangeable cogs in a vast,
> incomprehensible, relentless machine. The functioning of the economy
> as a whole became opaque, so it was impossible to see the meaning of
> one's own work, and the system's demands seemed senseless. And, indeed,
> working conditions often were not only awful, but pointlessly awful.

https://meaningness.com/systems-crisis-breakdown

<img style="max-width: 100%" src="assets/hire/02b80bbb9f554259-notext.jpg">

> Smart young things joining the workforce soon discover that, although
> they have been selected for their intelligence, they are not expected
> to use it. They will be assigned routine tasks that they will
> consider stupid. If they happen to make the mistake of actually
> using their intelligence, they will be met with pained groans from
> colleagues and polite warnings from their bosses. After a few years
> of experience, they will find that the people who get ahead are the
> stellar practitioners of corporate mindlessness.

> At the outset of our research, we suspected that organisational life
> would be full of stupidities. But we were genuinely surprised that
> otherwise smart people would go along with collective stupidity, and
> be rewarded for doing so.

https://aeon.co/essays/you-don-t-have-to-be-stupid-to-work-here-but-it-helps

<img style="max-width: 100%" src="assets/hire/d02490b7d5bf31df.jpg">

> Jobs have become, for so many, a relentless, unsatisfying toil.
> Why then does the work ethic still hold so much sway?
> - by [Jamie McCallum](https://www.jamiekmccallum.com/)

https://aeon.co/essays/how-the-work-ethic-became-a-substitute-for-good-jobs The Tyranny of Work

""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

if __name__ == "__main__":

	inparagraphs = text.split("\n\n")
	paragraphs = []

	config = _config.get_config()
	config['title'] = inparagraphs[0].split(None, 1)[1]

	for paragraph in inparagraphs[1:]:
		if paragraph.startswith("#"):
			hlevel, text = paragraph.split(None, 1)
			hlevel = len(hlevel)
			paragraphs.append("\n<h{0}>{1}</h{0}>".format(hlevel, text))
		elif paragraph.startswith("> "):
			paragraphs.append("\n<blockquote>{}</blockquote>".format(
				"\n".join(paragraph[2:].split("\n> "))))
		elif paragraph.startswith(("http://", "https://")):
			url_text = paragraph.split(maxsplit=1)
			if len(url_text) == 1:
				paragraphs.append(str(html.Anchor(paragraph)))
			else:
				paragraphs.append(str(html.Anchor(url_text[0], url_text[1])))
		else:
			paragraphs.append("\n<p>{}</p>".format(paragraph))

	config['sections'] = [paragraphs]

	main(config)
