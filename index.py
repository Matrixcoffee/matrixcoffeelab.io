import _config
import _template
from _template import tag
import sys

import transformat.parser
import indenttext

def insert_rel_me(html):
	if html.startswith("<a href"):
		return "<a rel=\"me\"" + html[2:]
	return html

def build_document(config):
	s_manifesto = tag("section", attrs={"class": "manifesto"})
	bq = tag("blockquote")
	s_manifesto.append(bq)
	bq.append(tag("p").append("The net belongs to us. To people. Not to billion-dollar corporations which gained those billions over our backs."))
	bq.append(tag("p").append("Organizations with members have power. So don't give any time, attention or money to those who seek to exploit us. Instead, give it to people and organizations which place empowering people far above their own wallets."))
	quit = tag("ul")
	bq.append(quit)
	for badthing in "Facebook Twitter Youtube Gmail Instagram TikTok Netflix Spotify".split():
		quit.append(tag("li").append("Quit {}".format(badthing)))

	ilst = _template.tag("ul")
	ctbl = _template.tag("table")

	s_index = tag("section", attrs={"class": "index"}) \
		.append(tag("h2").append("Index"),
			ilst)

	s_contact = tag("section", attrs={"class": "contact"})
	s_contact.append(	tag("h2").append("Contact"),
				ctbl)

	s_construction = tag("section", attrs={"class": "construction"})
	s_construction.append(	tag("p").append(tag("picture").append(
					tag("source", media="(min-width: 465px)", srcset="assets/cuc/BaBajaCanyon9450construct4.gif"),
					tag("source", media="(min-width: 170px)", srcset="assets/cuc/kikingsnakesmoonMealInABowlneonconstruction.gif"),
					tag("img", src="assets/cuc/BaBajaTrails8723construct.gif", alt="WARNING: this page is under construction"))))

	s_qualifier = tag("section", attrs={"class": "qualifier"})
	s_qualifier.append(tag("hr"))
	s_qualifier.append(tag("p").append("This website is made with love, but it is also made in my spare time, intermittently, with what energy remains after doing the work that pays for food and a roof over my head. Not to mention that I have far too many interests."))

	sections = indenttext.IndentText(indent="").append(
		s_manifesto,
		"",
		s_construction,
		"",
		s_index,
		"",
		s_contact,
		"",
		s_qualifier)

	config['footer'] = tag("img", src="assets/cuc/susunsetstripvine8672constructrib.gif", alt="Boycott Construction Signs - Orange Ribbon Campaign")

	for thing in "appideas.py lynx.py sixel-graphics.py".split():
		title = thing
		thong = thing
		if thong.endswith(".py"): thong = thong[:-3] + ".html"
		with open(thing, "r", encoding='UTF-8') as f:
			for i, line in enumerate(f):
				if line.startswith("# "):
					title = line[2:].strip()
					break
				if i >= 10: break
		li = tag("li", flow=True)
		a = tag("a", href=thong)
		a.append(title)
		li.append(a)
		ilst.append(li)

	with open("Website.org", "r", encoding='UTF-8') as f:
		incontact = False
		for line in f:
			line = line.rstrip()
			if line.strip() == "": continue
			if line.startswith("* "):
				incontact = (line == "* Contact")
				continue
			if incontact:
				if line.startswith("** "):
					method = transformat.parser.OrgElementParser.parse(line[3:])
				else:
					destination = transformat.parser.OrgElementParser.parse(line)

					dprbadge = ' <span class="tomato badge">deprecated</span>'
					if not method.as_html().startswith("<s>"): dprbadge = ""

					c = tag("tr", flow=True)
					ctbl.append(c)
					r = tag("td", flow=True)
					c.append(r)
					r.append(method.as_html() + ":")
					r = tag("td", flow=True)
					c.append(r)
					r.append(destination.as_html() + dprbadge)

	return _template.apply_template(config, sections)

def main(config):
	document = build_document(config)
	for line in document:
		print(line)

if __name__ == "__main__":
	config = _config.get_config()
	config['title'] = "Coffee's Website"
	config['byline'] = indenttext.IndentText(indent="") \
		.append("Doing it ",
		tag("a", href="https://cheapskatesguide.org/articles/old-internet-coming-back.html").append("old school"),
		"like it's going out of style.")

	main(config)
