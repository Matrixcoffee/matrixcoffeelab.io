import _builder
from _builder import html
import _config
import sys

s_recapps = [
 _builder.mklist("{}\n{}\n{}".format(
	"Browser Intercept - Share URL (Peek at urls)",
	html.Anchor("https://f-droid.org/app/com.intrications.android.sharebrowser",
		"(archived - only opens in the F-Droid app)"),
	html.Anchor("https://f-droid.org/wiki/page/com.intrications.android.sharebrowser",
		"(wiki)")),
 ordered=False)
]

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

if __name__ == "__main__":
	config = _config.get_config()
	config['title'] = "Recommended Apps"
	config['sections'] = s_recapps

	main(config)
