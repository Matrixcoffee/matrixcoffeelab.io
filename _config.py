import os
import sys

def _get_level():
	return os.path.normpath(sys.argv[0]).count(os.path.sep)

def get_config():
	config = {
		'lang': "en",
		'level': 0,
		'footer': "👣"
	}

	config['level'] = _get_level()
	config['relative_root'] = "../" * config['level']

	return config
