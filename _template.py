import indenttext

def tag(tagname, **attrs):
	if tagname in "html".split() and 'indent' not in attrs: attrs['indent'] = ""
	if tagname in "meta link source img".split() and 'container' not in attrs: attrs['container'] = False
	if tagname in "title h1 h2 h3 h4 a strong em".split() and 'flow' not in attrs: attrs['flow'] = True
	return indenttext.HTMLTag(tagname, **attrs)

def apply_template(config, sections):
	template = indenttext.IndentText(indent="")
	template.append("<!DOCTYPE html>")
	h = tag("html", lang="en")
	template.append(h)
	x = tag("head")
	h.append(x)
	x.append(tag("meta", charset="UTF-8"))
	x.append(tag("meta", name="viewport", content="width=device-width, initial-scale=1.0"))
	x.append(tag("link", rel="stylesheet", type="text/css", href="{relative_root}style.css".format(**config)))
	x.append(tag("title").append(config['title']))
	h.append("")

	b = tag("body")
	h.append(b)
	hdr = tag("header")
	b.append(hdr)
	hdr.append(tag("h1").append(config['title']))
	if 'byline' in config:
		hdr.append(tag("p", attrs={"class": "byline"}).append(config['byline']))
	b.append("")
	b.append(*sections)
	b.append("")
	b.append(tag("footer").append(config['footer']))

	return template
