class IndentedText:
	def __init__(self):
		self.indent = 0
		self.indenttext = ""
		self.lines = []

	def print(self, *args, sep=' ', end='\n', indent=None):
		txt = sep.join(args) + end
		idtxt = self.get_indenttext(indent)
		lines = txt.splitlines(True)
		for line in lines:
			self.lines.append(idtxt + line)

	def tprint(self, template, keywords, indent=None):
		return self.print(template.format(**keywords), sep="", indent=indent)

	def ntprint(self, template, keywords, indent=None):
		return self.print(template.format(**keywords), sep="", end="", indent=indent)

	def get_indenttext(self, indent=None):
		if indent is None: return self.indenttext
		return " " * indent

	def set_indent(self, indent):
		if indent != self.indent:
			self.indent = indent
			self.indenttext = " " * indent

	def __str__(self):
		return "".join(self.lines)
