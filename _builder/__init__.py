import sys

from .indent import IndentedText

def g(item):
	def r(obj, item=item):
		return obj[item]

	return r

template = [
 '<!DOCTYPE html>',
 '<html lang="en">',
 '<head>', [
  '<meta charset="UTF-8">',
  '<meta name="viewport" content="width=device-width, initial-scale=1.0">',
  '<link rel="stylesheet" type="text/css" href="{relative_root}style.css">',
  "<title>{title}</title>"],
 '</head>',
 '',
 '<body>', [
  '<header>', [
   "<h1>{title}</h1>", ],
  '</header>'],
 '',
 g("sections"),
 '',
 ['<footer>{footer}</footer>'],
 '</body>',
 '</html>'
]

def apply_template_recursively(templatedoc, config):
	for i, l in enumerate(templatedoc):
		if isinstance(l, str):
			if isinstance(templatedoc, list):
				templatedoc[i] = l.format(**config)
		elif isinstance(l, (list, tuple)):
			apply_template_recursively(l, config)
		elif callable(l) and isinstance(templatedoc, list):
			templatedoc[i] = l(config)
		else:
			raise Exception("Don't know how to handle {!r} in {!r}!".format(l, templatedoc))
	return templatedoc

def _render_recursively(itxt, doc, indent):
	for item in doc:
		if isinstance(item, str):
			itxt.print(item, indent=indent)
		else:
			_render_recursively(itxt, item, indent + 1)

def render_structure(doc):
	print("render_structure:", file=sys.stderr)
	print(repr(doc), file=sys.stderr)

	itxt = IndentedText()
	_render_recursively(itxt, doc, 0)
	return str(itxt)

def mktable(*args, **kwargs):
	r = []
	t = map(lambda x: x.split("\t"), args)

	for row in t:
		r.append("<tr><td>" + "</td><td>".join(row) + "</td></tr>")

	if r:	return ("<table>", r, "</table>")
	else:	return None

def mklist(*args, ordered=False):
	listargs = ["<li>{}</li>".format(x) for x in args]
	return [
		('<ul>', '<ol>')[ordered],
		listargs,
		('</ul>', '</ol>')[ordered],
	]

def generate_html_doc(config):
	doc = apply_template_recursively(template, config)
	return render_structure(doc)
