class BaseTag:
	pass

class Anchor(BaseTag):
	def __init__(self, url, text=None):
		self.url = url
		self.text = text
		if text is None: self.text = url

	def __str__(self):
		return '<a href="{0.url}">{0.text}</a>'.format(self)

class List(BaseTag):
	def __init__(self, *items, ordered=False):
		self.items = items
		self.ordered = bool(ordered)

	def struct(self):
		listargs = ["<li>{}</li>".format(x) for x in self.items]
		return [
			('<ul>', '<ol>')[ordered],
			listargs,
			('</ul>', '</ol>')[ordered],
		]
