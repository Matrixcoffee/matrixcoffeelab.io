import _builder
import _config
import sys

import markdumb

text = """
# Optimizing Lynx

I like [Lynx](https://lynx.invisible-island.net/) as an everyday browser, but its prehistoric key bindings are a pain. Fortunately, we can change them.

## keymap

Let's start with some saner keybindings for the arrow keys. We'll change the up and down arrows to unconditionally scroll the text two lines up or down. Not only is this more intuitive, but it also works better with the scroll wheel and touchscreen drag gestures. Why not one line, like in "less"? Lynx *cannot* scroll up or down by a single line. Seriously.

Next we'll redefine the left and right arrow keys to do what up and down did previously. That is, navigate to the previous and next links. Much saner. Arrow keys should be used for movement, not actions, especially not actions that trigger page loads.

Now it's time for some shuffling. Opening links was already duplicated under the Enter key, which is a good key for that. Unfortunately there is no intuitive alternative for navigating back, which was previously mapped under the left arrow key. Let's put that under backspace/delete instead. Ah, but because that already opened the history window, we'll place history under the 'h' key, overwriting the help. This is fine, because the same help function remains available under capital 'H' and '?'.

Finally, we'll make the spacebar scroll by half a page instead of a whole one, so we can keep some context in view while reading.

To achieve all this, we'll add the following to `~/.lynx/keymap`:

```
KEYMAP:UPARROW:UP_TWO		# Move display up two lines
KEYMAP:DNARROW:DOWN_TWO		# Move display down two lines
KEYMAP:0x20:DOWN_HALF		# Move display down half a page
KEYMAP:LTARROW:PREV_LINK	# Move to the previous link or page
KEYMAP:RTARROW:NEXT_LINK	# Move to the next link or page
KEYMAP:h:HISTORY		# Show the history list
KEYMAP:0x08:PREV_DOC		# Return to the previous document in history stack
KEYMAP:0x7F:PREV_DOC		# Return to the previous document in history stack
```

([download](/assets/lynx/keymap.in))

You can also download a ([full keymap file](/assets/lynx/keymap)), which is the relevant subsection of `lynx.cfg` with the above values edited in.

## lynx.cfg

Now, a few more things. To make sure scrolling doesn't snag on text fields, we'll set TEXTFIELDS_NEED_ACTIVATION to TRUE. This means you can no longer simply start typing when a text field is selected, and need to press the Enter key first. Inconvenient, but the alternative is awful scrolling behavior.

Let's also make images downloadable by default, show the url of the currently selected link in the bottom status bar, and some other niceties.

Finally, we'll tighten up privacy by turning off cookies and the Referer: header. **Caution**: Many websites already don't work (well) with Lynx. This will break even more of them.

To achieve all this, add the following to `/etc/lync/local.cfg`:

```
SET_COOKIES:FALSE
ACCEPT_ALL_COOKIES:FALSE
DEFAULT_USER_MODE:ADVANCED
NO_FROM_HEADER:TRUE
NO_REFERER_HEADER:TRUE
NO_FILE_REFERER:TRUE
MAKE_LINKS_FOR_ALL_IMAGES:TRUE
SUBSTITUTE_UNDERSCORES:TRUE
TEXTFIELDS_NEED_ACTIVATION:TRUE
```

([download](/assets/lynx/lynx.cfg.in))

Or you can run [this awk script](/assets/lynx/lynxcfg.awk) to automatically edit them directly into `lynx.cfg`. See inside the script for usage.

~
""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

def paragraphize(iterable):
	for paragraph in iterable:
		if paragraph.startswith("<"): yield paragraph
		elif paragraph.startswith("```\n") and paragraph.endswith("\n```"):
			yield "<pre>{}</pre>\n".format(paragraph[3:-3])
		else: yield "<p>" + paragraph + "</p>\n"

if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	config['sections'] = [paragraphs]

	main(config)
