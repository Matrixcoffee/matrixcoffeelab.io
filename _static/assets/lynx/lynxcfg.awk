# Usage:

# awk -f lynxcfg.awk < /etc/lynx/lynx.cfg > lynx.cfg.new

# To verify:

# diff -u /etc/lynx/lynx.cfg lynx.cfg.new

# When satisfied:

# cp lynx.cfg.new /etc/lynx/lynx.cfg # (as root)

BEGIN {
	A["SET_COOKIES"] = "SET_COOKIES:FALSE"
	A["ACCEPT_ALL_COOKIES"] = "ACCEPT_ALL_COOKIES:FALSE"
	A["DEFAULT_USER_MODE"] = "DEFAULT_USER_MODE:ADVANCED"
	A["NO_FROM_HEADER"] = "NO_FROM_HEADER:TRUE"
	A["NO_REFERER_HEADER"] = "NO_REFERER_HEADER:TRUE"
	A["NO_FILE_REFERER"] = "NO_FILE_REFERER:TRUE"
	A["MAKE_LINKS_FOR_ALL_IMAGES"] = "MAKE_LINKS_FOR_ALL_IMAGES:TRUE"
	A["SUBSTITUTE_UNDERSCORES"] = "SUBSTITUTE_UNDERSCORES:TRUE"
	A["TEXTFIELDS_NEED_ACTIVATION"] = "TEXTFIELDS_NEED_ACTIVATION:TRUE"
}

{
	l = $0
	sub("^#", "", l)
	gsub(":.*$", "", l)

	if (l in A) {
		print A[l]
		delete A[l]
	} else {
		print
	}
}

END {
	h = 1
	for (l in A) {
		if (h) {
			print ""
			print "# [lynxcfg.awk] Some config settings were not found."
			print "# [lynxcfg.awk] Setting them here:"

			h = 0
		}

		print A[l]
	}
}
