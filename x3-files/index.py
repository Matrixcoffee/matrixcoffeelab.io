import _builder
import _config
import sys

text = """
# The X3 Files

**eXploit—eXtract—eXterminate**

## Microsoft

## PayPal

""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

if __name__ == "__main__":

	inparagraphs = text.split("\n\n")
	paragraphs = []

	config = _config.get_config()
	config['title'] = inparagraphs[0].split(None, 1)[1]

	for paragraph in inparagraphs[1:]:
		if paragraph.startswith("#"):
			hlevel, text = paragraph.split(None, 1)
			hlevel = len(hlevel)
			paragraphs.append("\n<h{0}>{1}</h{0}>".format(hlevel, text))
		else:
			paragraphs.append("\n<p>{}</p>".format(paragraph))

	config['sections'] = [paragraphs]

	main(config)
