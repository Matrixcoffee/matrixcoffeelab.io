import _builder
import _config
import sys

import markdumb

text = """
# Ideas for Apps and Programs

Because I have no [large time blocks available for creative work](http://paulgraham.com/makersschedule.html) right now but I'm still thinking about things. Hopefully I can pick these up later, or someone else might pick up an idea and start a project.

## The Shopping Collection

One or several Android apps which are able to collaborate for the purpose of shopping and keeping track of PFF* inventory.

__*PFF__: Pantry/Fridge/Freezer

### Products and Ingredients

In particular, these tools should understand the difference between products:

* "Uncle Harry's Grated Gouda, 150g flowpack, EAN 2057260892673"

and ingredients:

* "75 grams grated 30+ cheese"

and use tags/properties to mediate between them:

* Cheese **#grated #30+ #old**

and perhaps also properties, so you can do comparisons, e.g. ripening time between 40 and 60 months.

### Inventory

Possibly as a separate app, keep track of inventory, expiration dates, and of setpoints (minimum and maximum number of toilet paper rolls we want to have in stock). If we integrate a barcode scanner, products can be quickly added and removed. Then we can add a guided "before-shopping" walkthrough through our inventory, and automatically add items to the shopping list based on available inventory and its setpoints.

Inventory could also keep track of prices of products at various stores. Then, it could generate shopping lists for each store so that we buy each item at the cheapest possible price. Or, we could tell it we are only willing to visit 1 store, and it could tell us which store to shop at to get the overall lowest price for our shopping list. This could also be generalized for an n-store limit.

### Shopping

TODO, but should be fairly straightforward.

### Recipes

An app to keep track of recipes, and which can automatically add ingredients to the companion shopping list app.

Or maybe we can just integrate a share target into the shopping list app, and use e.g. a specially formatted markdown file which we can write up in, and share from, e.g. Markor.

### Store Mapper

App to make a connectivity graph of aisles and/or draw a floor plan for each known store. This can then be used to automatically sort the shopping list according to the optimal route through the store.

## HyperCard

Apparently the only existing software in its class. What is it, why is it, why is there no modern FOSS reimplementation of it, and what could that look like for Linux and/or Android?

One obstacle: Patents.

> "The patent for what became HyperCard was called "Search/retrieval system" and was held by Paul Henckel. It only expired in 2005: [https://patents.google.com/patent/US4736308](https://patents.google.com/patent/US4736308)"

--[@cypnk](https://mastodon.social/@cypnk/105927202144603812)

~
""".strip()

def main(config):
	sys.stdout.buffer.write(_builder.generate_html_doc(config).encode("UTF-8"))

def paragraphize(iterable):
	for paragraph in iterable:
		if paragraph.startswith("<"): yield paragraph
		else: yield "<p>" + paragraph + "</p>\n"

if __name__ == "__main__":
	html = markdumb.as_html(text)
	paragraphs = paragraphize(html.split("\n\n")[1:-1])

	config = _config.get_config()
	config['title'] = text.split("\n", 1)[0].split(None, 1)[1]

	config['sections'] = [paragraphs]

	main(config)
